const http = require("http") // importing module

const port = 3000 // adding variable for port

const server = http.createServer((request, response) => {

	if (request.url == "/login"){
	response.writeHead(200, {'Content-Type': "text/plain"});
	response.end("Welcome to the login page!");
	}
	else {
		response.writeHead(404, {'Content-Type': "text/plain"});
		response.end("I'm sorry. The page you are looking for cannot be found.");
	}

})

server.listen(port) // will listen to port
console.log(`The server is running in port: ${port}.`);